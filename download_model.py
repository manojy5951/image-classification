import os
import sys
from six.moves import urllib
import tarfile
# check if model file is downloaded , if not proceed to download

DATA_URL = 'http://download.tensorflow.org/models/image/imagenet/inception-2015-12-05.tgz'


def check_model_download(path):
    """Download and extract model tar file."""
    dest_directory = path
    if not os.path.exists(dest_directory):
        os.makedirs(dest_directory)
    filename = DATA_URL.split('/')[-1]
    filepath = os.path.join(dest_directory, filename)
    if not os.path.exists(filepath):
        def _progress(count, block_size, total_size):
            sys.stdout.write('\r>> Downloading %s %.1f%%' % (
                filename, float(count * block_size) / float(total_size) * 100.0))
            sys.stdout.flush()

        filepath, _ = urllib.request.urlretrieve(DATA_URL, filepath, _progress)
        print()
        statinfo = os.stat(filepath)
        print('Successfully downloaded', filename, statinfo.st_size, 'bytes.')
        # if not os.path.exists(os.path.join(dest_directory,'model')):
        #
        #     os.makedirs(os.path.join(dest_directory,'model'))
        tarfile.open(filepath, 'r:gz').extractall(os.path.join(dest_directory))

if __name__ == '__main__':
    check_model_download(os.getcwd())
